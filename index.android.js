/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { AppRegistry } from 'react-native';
import ChatterBoxxApp from './app/index';

AppRegistry.registerComponent('ChatterBoxxApp', () => ChatterBoxxApp);